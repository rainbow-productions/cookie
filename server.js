// server.js

const express = require('express');
const { exec } = require('child_process');

const app = express();

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

app.get('/launch', (req, res) => {
  exec('google-chrome --remote-debugging-port=9222');
  res.send('Launched Chrome!');
});

app.get('/browse', (req, res) => {
  const url = req.query.url;
  exec(`google-chrome --remote-debugging-port=9222 --new-window ${url}`);
  res.send(`Browsed to ${url}`);
});

app.listen(3000, () => {
  console.log('Server listening on port 3000'); 
});
